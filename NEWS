Changelog for obarun-install

In 2.2.2
-------

    - Pass to git.obarun.org as source

In 2.2.1
--------

    - Fix mirrorlist feature

In 2.2.0
--------

    - Bugs fix
    - Pass to new service of type module

In 2.1.3
--------

    - Fix glibc installation.
    - Do not use run_cmd() at script udpate time.

In 2.1.2
--------

    - Fix gpg directory creation.

In 2.1.1
--------

    - Fix database synchronization issue

In 2.1.0
--------

    - Bugs fix.
    - Use obsysusers instead of applysys.
    - Use oblog for log message: /tmp/obarun-install.log is now created.
    - Always check {archlinux,obarun}-keyring and glibc version.
    - Always use pacman.conf from the installer.
    - Improve Makefile file.

In 2.0.3
--------

    - Fix bootloader installation:
        - Grub is now provided by obcore
        - Simplify syslinux installation
    - Rerun automatically obarun-install after an update
    - Check the size of the screen
    - Some message output fix and dialog behavior

In 2.0.2
--------

    - Hot fix: fix grub installation

In 2.0.1
--------

    - Hot fix: add pacman-contrib as dependency

In 2.0.0
--------

    - Implement dialog
    - Implement guided partitionning and manual partitionning
    - Automatization of UEFI installation

In 1.0.3
--------

    - Convenient update
    - Use new version of 66(0.2.1.2)

In 1.0.2
--------

    - Added expert option "Firmware interface"
    - Offer automatic bootloader installation for UEFI using GRUB or EFISTUB
    - Change 66.conf configuration to boot.conf file

In 1.0.1
--------

    - Bugs fix
    - Add new variable BRANCH_THEMES and GIT_ADDR_THEMES to install.conf file

In 1.0.0
--------

    - Allow grub bootloader installation

In 0.9.9
--------

    - Use db path for pacman

In 0.9.8
--------

    - Pass to 66
    - Use of fzf for interactive menu

In 0.9.6
--------

    - Bugs fix
    - Change github address to framagit
    - Allow to specify the branch to use on update process
    - Edit the syslinux.cgf automatically in fonction of the mountpoint
    used
    - Prepare pacman sysroot change

In 0.9.5
--------

    - Bugs fix
    - Add warm message feature : be able to display important informations
      at the beginning of the installation
    - Add Quick install feature : Copy the ISO as it on the DD
    - Reorganize flow of the script and main menu to not confusing the user

In 0.9.4
-------

    - Nothing, cosmetic version

In 0.9.3
--------

    - Bugs fix
    - Allow multiple generation of locale
    - Copy AUR builded package to cache directory

In 0.9.2
--------

    - Bugs fix
    - Add rankmirrors features
    - Add virtualbox detection
    - Force to restart the script when obarun-install is upgraded

In 0.9.1
--------

    - Pass under ISC license

